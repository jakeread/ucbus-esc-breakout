## 2020 09 14 

Notes for attempt to speed control 'esc' type controller, using PWM output and hall reading input. 

OK, have verified that the hall readout is as expected, so I will be able to count pulses. 14 per revolution. So I can solder up another test-esc-board, and get into trying to read that & control it. 

This is set up now, so I have to configure a timer on PB14, and get setup to debug / etc... 

First observation is that: hot damn: at 60kRPM, with 14 ticks per rev, this signal is going to be 50 mHz? Seems wild... op, nevermind, that was /60 not *60, so that'd be 14kHz, god bless. I do need a pretty high resolution timer here though, I should want a lower bound of measurement at about 500RPM, upper bound 60kRPM. TC outputs 0 or 1 can both be input / output for the counter, I think, that's good. I'm not sure I have timer setup code, except for internal interrupt generators. 

My tack is to do this: setup the timer, capture channel edges, and put counts in a ringbuffer of ~ 16 values. When I want to read that, I'll average it and pull an RPM value, then do PID over that. I'll want to generate an internal clock to run that PID loop on as well. 

So, about timer resolution. I have 16 bits. The slowest speed I'd like to measure is 500rpm, which would generate ~ 100Hz signal, so I should aim for my 16 bit timer to wrap at 100Hz - so that's 6553600 ticks / s for 100hz wrap, about 6MHz clock - so I can do the 16MHz source / 4, to get well underneath. At 60kRPM, I'll still have ~ 285 counts in the timer, enough resolution. 

I think the way to do this is to setup two interrupts: one on the capture (defaults to the rising edge capture), where I would store the count and then reset it, and one on the overflow, where I would store the full width (lowest speed reading) and reset it. 

OK, I think I just need to setup some debug pins, to make sure I am capturing the overflow and capture interrupts properly. 

OK, I think this is set, and I'm seeing the overflow interrupt OK, but not the capture channel. Op, just had a pin misconfigured... 14, not 16. 

So, next I'll read captures into a ringbuffer, then try printing out speeds... do with modular div/ count, yar? 

### Counting Ticks 

Here's the setup code, to track:

```cpp
    // setup PB14 to this peripheral 
    PULSE_PORT.DIRCLR.reg = PULSE_PIN_BM;   // not-output 
    PULSE_PORT.PINCFG[PULSE_PIN].bit.PMUXEN = 1; // allow peripheral mux 
    if(PULSE_PIN % 2){
        PULSE_PORT.PMUX[PULSE_PIN >> 1].reg |= PORT_PMUX_PMUXO(PERIPHERAL_E);
    } else {
        PULSE_PORT.PMUX[PULSE_PIN >> 1].reg |= PORT_PMUX_PMUXE(PERIPHERAL_E);
    }
    // setup the xtal, will use to send a clock to this timer 
    d51_clock_boss->setup_16mhz_xtal();
    // reset
    TC5->COUNT16.CTRLA.bit.ENABLE = 0;
    // unmask clocks 
    MCLK->APBCMASK.reg |= MCLK_APBCMASK_TC5;
    // send a clock to the ch 
    GCLK->PCHCTRL[TC5_GCLK_ID].reg = GCLK_PCHCTRL_CHEN 
        | GCLK_PCHCTRL_GEN(d51_clock_boss->mhz_xtal_gclk_num);
    // setup timer 
    TC5->COUNT16.CTRLA.reg = TC_CTRLA_MODE_COUNT16 // 16 bit timer 
        | TC_CTRLA_PRESCSYNC_PRESC  // reload / reset on prescaler 
        | TC_CTRLA_PRESCALER_DIV256   // div the input clock, 
        | TC_CTRLA_CAPTEN0          // enable for capture option on ch0 
        | TC_CTRLA_COPEN0;          // capture on pin 
    // capture event defaults to the rising edge, that's OK. 
    // we need to get a capture and overflow interrupt, 
    TC5->COUNT16.INTENSET.bit.MC0 = 1;
    TC5->COUNT16.INTENSET.bit.OVF = 1;
    // now enable it, 
    while(TC5->COUNT16.SYNCBUSY.bit.ENABLE);
    TC5->COUNT16.CTRLA.bit.ENABLE = 1;
    // enable the IRQ
    NVIC_EnableIRQ(TC5_IRQn);
```

Then interrupts:
```cpp
void TC5_Handler(void){
    if(TC5->COUNT16.INTFLAG.bit.MC0){
        // clear, stash, and reset 
        TC5->COUNT16.INTFLAG.bit.MC0 = 1; // clear 
        DEBUG1PIN_TOGGLE;
    }
    if(TC5->COUNT16.INTFLAG.bit.OVF){
        // clear, stash, and reset 
        TC5->COUNT16.INTFLAG.bit.OVF = 1;
        DEBUG2PIN_TOGGLE;
    }
}
```

Next I'll actually do something inside those interrupts, then see about some beginner PID params. 

OK, capture happens on rising edges, so I have 7 of those per rev. 

Got that all running, code is like:

```cpp
void TC4_Handler(void){
    if(TC4->COUNT16.INTFLAG.bit.MC0){
        uint16_t width;
        pulse_counter->_lastWasOVF ? width = 65534 : width = TC4->COUNT16.CC[0].reg;
        pulse_counter->_lastWasOVF = false;
        // clear, stash, and reset 
        TC4->COUNT16.INTFLAG.bit.MC0 = 1; // clear 
        pulse_counter->addPulse(width); // stash 
        TC4->COUNT16.CTRLBSET.reg = TC_CTRLBSET_CMD_RETRIGGER; // restart (?) 
        DEBUG1PIN_TOGGLE;
    }
    if(TC4->COUNT16.INTFLAG.bit.OVF){
        // clear, stash, reset is already happening 
        pulse_counter->_lastWasOVF = true;
        TC4->COUNT16.INTFLAG.bit.OVF = 1;
        pulse_counter->addPulse(65534); // the long nap 
        DEBUG2PIN_TOGGLE;
    }
}

void Pulse_Counter::addPulse(uint16_t width){
    _widths[_wh] = width;
    _wh ++; 
    if(_wh >= PULSE_WIDTHS_COUNT){
        _wh = 0;
    }
}

float Pulse_Counter::getAverageWidth(void){
    float sum = 0;
    NVIC_DisableIRQ(TC4_IRQn);
    for(uint8_t i = 0; i < PULSE_WIDTHS_COUNT; i ++){
        sum += _widths[i];
    }
    NVIC_EnableIRQ(TC4_IRQn);
    return sum / (float)PULSE_WIDTHS_COUNT;
}
```

### PWM Output

Next up I need to generate the PWM signal. I had previously done this on a big banged loop... don't want that, I should properly scope the thing out and run it with a timer I think. I should write a little servo PWM class then... 

*Unfortunately* I put the PWM signal on this board on PA22, where it's TC4 out, same as the hall I'm currenty reading... so I could modify the CAD and put the hall back on TC5-1, or I could carry on and use an interrupt to flip the pin... that's a bit of a hack, but makes muxing easy in software and servo PWM is slow AF anyways, 20ms cycle with 1-2ms pulse widths, so an interrupt is cool here. 

OK, have that set, no problems. Since I am anticipating wanting to do this again...

```cpp
#define PWM_PORT PORT->Group[0]
#define PWM_PIN 22 
#define PWM_PIN_BM (uint32_t)(1 << PWM_PIN)
#define PWM_PIN_HI PWM_PORT.OUTSET.reg = PWM_PIN_BM
#define PWM_PIN_LO PWM_PORT.OUTCLR.reg = PWM_PIN_BM

// PWM for servos is 20ms period, 1-2ms duty.
// do 2MHz input clock, wrap at 40000 ticks 
// 1ms is 2000 ticks, 2ms is 4000 ticks 

void Servo_PWM::init(void){
    // set the pin as output: this is toggled on interrupt, not hardware TC 
    PWM_PORT.DIRSET.reg = PWM_PIN_BM;
    // setup xtal for timer clk 
    d51_clock_boss->setup_16mhz_xtal();
    // disable to setup 
    TC3->COUNT16.CTRLA.bit.ENABLE = 0;
    // unmask clock 
    MCLK->APBBMASK.reg |= MCLK_APBBMASK_TC3;
    // send clk to ch 
    GCLK->PCHCTRL[TC3_GCLK_ID].reg = GCLK_PCHCTRL_CHEN
        | GCLK_PCHCTRL_GEN(d51_clock_boss->mhz_xtal_gclk_num);
    // setup timer 
    TC3->COUNT16.CTRLA.reg = TC_CTRLA_MODE_COUNT16
        | TC_CTRLA_PRESCSYNC_PRESC
        | TC_CTRLA_PRESCALER_DIV8;
    // want match pwm (MPWM) mode, should have a TOP register for period, and CC[x] for the swap 
    TC3->COUNT16.WAVE.reg = TC_WAVE_WAVEGEN_MPWM;
    TC3->COUNT16.CC[0].reg = 40000; // CC0 is 'top' in match pwm 
    TC3->COUNT16.CC[1].reg = 2000; // this should put the first tick at 1ms 
    // want interrupts
    TC3->COUNT16.INTENSET.bit.MC0 = 1;
    TC3->COUNT16.INTENSET.bit.MC1 = 1;
    // the rest 
    // ...
    // now enable 
    while(TC3->COUNT16.SYNCBUSY.bit.ENABLE);
    TC3->COUNT16.CTRLA.bit.ENABLE = 1;
    // enable the IRQ
    NVIC_EnableIRQ(TC3_IRQn);
}

// need to finish setup above, to do... something, I think compare at duty cycle and wrap at 20ms
// then try write(0) and write(1) at some intervals in loop,
// configure your debugs (currently triggered on pulse_counter events) and watch scope 
// then roll PID... tune... and do bus action 
// maybe tune after bus action, have pid-config there, not too hard 

void TC3_Handler(void){
    if(TC3->COUNT16.INTFLAG.bit.MC0){
        TC3->COUNT16.INTFLAG.bit.MC0 = 1;
        PWM_PIN_HI;
        DEBUG1PIN_ON;
    }
    if(TC3->COUNT16.INTFLAG.bit.MC1){
        TC3->COUNT16.INTFLAG.bit.MC1 = 1;
        PWM_PIN_LO;
        DEBUG1PIN_OFF;
        DEBUG2PIN_TOGGLE;
    }
}
```

So now I want to roll a little PID loop. 

OK, I'm into 'tuning' and it's a pain. Trying to go with just the P term, obv not enough. I'm also routinely tripping my 3A fancy PSU, should hit it with the big one. And even with that, some strange micro resets are ongoing. I'll see about deleting the gnd line from micro-to-esc. 

I suspect (?) the ESC is just throwing noise into the system, causing resets. I also think it supports higher refresh rates, so I'll push that to improve my PID timing... 50hz probably not good enough. Also, spinde inertia will probably change this thing entirely, so I should mount it up before I continue. From there, I might just need to setup an interface to tune PIDs, so that I can do it without this code rewrite thing... but I do feel like getting it into the ballpark is a good move. 

Scaling error to 0-1 domain before running PID helps a tonne, but I am now doubting my rpm reading. Might be the case that motor outputs are flipping the HALL sensor, wouldn't be shocked by this. 

Yep, that's absolutely what's going on. So this project is shot, hall sensors do need to live inside the housing and, broadly, we want encoders and better BLDC control. I'll do the remainder with a linear scaling between 0 and 55k rpm for commands to write direct to the pwm output, no feedback. Makes wiring simpler, etc, and I won't have to chase down this bug that's causing things to reset (or maybe I will anyways!). 

Might still spin the bulid up with the grommets, to see if that makes things less-loud. 

## 2020 09 18

OK, totally sewered yesterday with a parts-lost-in-the-mail issue, meaning I have re-designed the whole spindle assembly. Moving on, I'm getting this set up to listen to remote requests for spindle speed. I think I'm going to do the mapping in JS, then just push 0-1 PWM values to the remote thing. 

OK, mapping in JS: good. This works. 