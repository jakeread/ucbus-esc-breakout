## UCBus ESC Breakout

This small circuit mounts under one of [these modules](https://gitlab.cba.mit.edu/jakeread/ucbus-module) to break out an off the shelf ESC and control a BLDC motor, in this case for a small spindle. 

![schem](log/2020-08-25_esc-schematic.png)
![route](log/2020-08-25_esc-routed.png)

## Hall Speed Sensor

I've made this small thing to locate some hall sensors around a 2207 BLDC motor. I'm hoping I can use these to measure speed / rough rotor position and close some speed control loops. 

| Part | PN |
| --- | --- |
| Grommets | 9600K171 |
| Hall Digital | 480-5195-ND | 
| Hall Analog | EQ731L-ND |
| IDC 6 Plug | 609-2842-ND |
| IDC 6 Header | 732-5394-ND‎ |
| 6 Ribbon Cable | 3M156095-25-ND‎ |

![schem](log/2020-08-25_halls-schematic.png)
![board](log/2020-08-25_halls-routed.png)
