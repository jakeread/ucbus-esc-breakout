/*
drivers/servo_pwm.cpp

output servo-type (20ms period, 1-2ms duty cycle) with TC on D51

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2020

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo
projects. Copyright is retained and must be preserved. The work is provided as
is; no warranty is provided, and users accept all liability.
*/

#include "servo_pwm.h"
#include "../utils_samd51/clock_utils.h"
#include "indicators.h"
#include "peripheral_nums.h"

Servo_PWM* Servo_PWM::instance = 0;

Servo_PWM* Servo_PWM::getInstance(void){
    if(instance == 0){
        instance = new Servo_PWM();
    }
    return instance;
}

Servo_PWM* servo_pwm = Servo_PWM::getInstance();

Servo_PWM::Servo_PWM(){}

#define PWM_PORT PORT->Group[0]
#define PWM_PIN 22 
#define PWM_PIN_BM (uint32_t)(1 << PWM_PIN)
#define PWM_PIN_HI PWM_PORT.OUTSET.reg = PWM_PIN_BM
#define PWM_PIN_LO PWM_PORT.OUTCLR.reg = PWM_PIN_BM

// PWM for servos is 20ms period, 1-2ms duty.
// do 2MHz input clock, wrap at 40000 ticks 
// 1ms is 2000 ticks, 2ms is 4000 ticks 

void Servo_PWM::init(void){
    // set the pin as output: this is toggled on interrupt, not hardware TC 
    PWM_PORT.DIRSET.reg = PWM_PIN_BM;
    // setup xtal for timer clk 
    d51ClockUtils->setup_16mhz_xtal();
    // disable to setup 
    TC3->COUNT16.CTRLA.bit.ENABLE = 0;
    // unmask clock 
    MCLK->APBBMASK.reg |= MCLK_APBBMASK_TC3;
    // send clk to ch 
    GCLK->PCHCTRL[TC3_GCLK_ID].reg = GCLK_PCHCTRL_CHEN
        | GCLK_PCHCTRL_GEN(d51ClockUtils->mhz_xtal_gclk_num);
    // setup timer 
    TC3->COUNT16.CTRLA.reg = TC_CTRLA_MODE_COUNT16
        | TC_CTRLA_PRESCSYNC_PRESC
        | TC_CTRLA_PRESCALER_DIV4;
    // want match pwm (MPWM) mode, should have a TOP register for period, and CC[x] for the swap 
    TC3->COUNT16.WAVE.reg = TC_WAVE_WAVEGEN_MPWM;
    TC3->COUNT16.CC[0].reg = 5000; // CC0 is 'top' in match pwm: 40k here for 20ms period, 5k for 2.5ms / 400hz
    TC3->COUNT16.CC[1].reg = 250; // this should put the first tick at 1ms 
    // want interrupts
    TC3->COUNT16.INTENSET.bit.MC0 = 1;
    TC3->COUNT16.INTENSET.bit.MC1 = 1;
    // the rest 
    // ...
    // now enable 
    while(TC3->COUNT16.SYNCBUSY.bit.ENABLE);
    TC3->COUNT16.CTRLA.bit.ENABLE = 1;
    // enable the IRQ
    NVIC_EnableIRQ(TC3_IRQn);
}

// need to finish setup above, to do... something, I think compare at duty cycle and wrap at 20ms
// then try write(0) and write(1) at some intervals in loop,
// configure your debugs (currently triggered on pulse_counter events) and watch scope 
// then roll PID... tune... and do bus action 
// maybe tune after bus action, have pid-config there, not too hard 

void TC3_Handler(void){
    if(TC3->COUNT16.INTFLAG.bit.MC0){
        TC3->COUNT16.INTFLAG.bit.MC0 = 1;
        PWM_PIN_HI;
        DEBUG1PIN_ON;
    }
    if(TC3->COUNT16.INTFLAG.bit.MC1){
        TC3->COUNT16.INTFLAG.bit.MC1 = 1;
        PWM_PIN_LO;
        DEBUG1PIN_OFF;
    }
}

void Servo_PWM::setDuty(float duty){
    // 0-1 -> 2000-4000
    if(duty > 1.0F){
        duty = 1.0F;
    } else if (duty < 0.0F){
        duty = 0.0F;
    }
    uint16_t ticks = duty * 250;
    TC3->COUNT16.CCBUF[1].reg = 250 + ticks; //2000 + ticks; 
}