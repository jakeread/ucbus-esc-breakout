#include <Arduino.h>

#include "indicators.h"
#include "drivers/servo_pwm.h"

#include "osape/core/osap.h"
#include "osape/vertices/endpoint.h"
#include "osape_arduino/vp_arduinoSerial.h"
#include "osape_ucbus/vb_ucBusDrop.h"

OSAP osap("esc-drop");

// -------------------------------------------------------- 0: USB Serial 

VPort_ArduinoSerial vpUSBSerial(&osap, "arduinoUSBSerial", &Serial);

// -------------------------------------------------------- 1: Bus Drop 

VBus_UCBusDrop vbUCBusDrop(&osap, "ucBusDrop"); 

// -------------------------------------------------------- 2: Spindle Duty (0-1 FP)

EP_ONDATA_RESPONSES onDutyData(uint8_t* data, uint16_t len){
  uint16_t rptr = 0;
  float duty = ts_readFloat32(data, &rptr);
  servo_pwm->setDuty(duty);
  // get a float, set a float
  return EP_ONDATA_ACCEPT;
}

Endpoint dutyEP(&osap, "spindleDuty", onDutyData);

// -------------------------------------------------------- Arduino Setup 

void setup() {
  ERRLIGHT_SETUP;
  CLKLIGHT_SETUP;
  // DEBUG1PIN_SETUP;
  // DEBUG2PIN_SETUP;
  // port begin 
  vpUSBSerial.begin();
  vbUCBusDrop.begin();
  // pwm-out, 
  servo_pwm->init();
  servo_pwm->setDuty(0.0F);
  // ERRLIGHT_ON;
  // CLKLIGHT_ON;
}

// 0.1 -> nok RPM
// 0.15 -> 2.5k RPM ~ unstable stops 
// 0.2 -> 6k RPM 
// 0.3 -> 12k RPM 6k comfortable, low harmonic 
// 0.4 -> 18k RPM 6k p loud 
// 0.5 -> 23k RPM 5k loud AF 
// 0.6 -> don't do this

// -------------------------------------------------------- Das Loop 

void loop() {
  osap.loop();
  // CLKLIGHT_TOGGLE;
} // end loop 

void ucBusDrop_onRxISR(void){
  // DEBUG1PIN_ON;
  // axl_integrator();
  // DEBUG1PIN_OFF;
}